//   ___    __________   |  Vasanth Developer (Vasanth Srivatsa)
//   __ |  / /___  __ \  |  ------------------------------------------------
//   __ | / / __  / / /  |  https://github.com/vasanthdeveloper/nirdesha.git
//   __ |/ /  _  /_/ /   |
//   _____/   /_____/    |  Entryfile for nirdesha project
//                       |

import Discord from 'discord.js'

import NirdeshaApp from './classes/NirdeshaApp'
import Interaction from './classes/Interaction/index'

async function app(name: string, client: Discord.Client): Promise<NirdeshaApp> {
    return new NirdeshaApp(name, client)
}

export default {
    app,
    Interaction,
}

export { default as Interaction } from './classes/Interaction/index'
