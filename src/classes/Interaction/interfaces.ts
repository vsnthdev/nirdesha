import Discord, { BitFieldResolvable, PermissionString } from 'discord.js'

export interface InteractionArgumentImpl {
    name: string
    identifier: string
    description: string
    type: string
    required: boolean
    multiple?: boolean
}

export interface InteractionConfigImpl {
    command: string
    description: string
    restricts: BitFieldResolvable<PermissionString>[]
    action: (message: Discord.Message, args: any) => Promise<boolean>
    arguments: InteractionArgumentImpl[]
}
