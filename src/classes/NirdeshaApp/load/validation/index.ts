/*
 *  This file will validate if imported code is a valid interaction that can be loaded
 *  or not.
 *  Created On 12 August 2020
 */

import Interaction from '../../../Interaction'

export default async function validation(
    code: any,
    file: string,
    directory: string,
): Promise<void> {
    // check if they export default the Interaction class
    if (!(code instanceof Interaction)) {
        throw Error(
            `The file "${file
                .replace(directory, '')
                .substring(1)}" doesn't export an Interaction.`,
        )
    }

    // make sure there is an action tired to that interaction
    if (!code.data.action) {
        throw Error(
            `The file "${file
                .replace(directory, '')
                .substring(1)}" doesn't have an action tied to it.`,
        )
    }
}
