/*
 *  This file will run the imported file through a transformer
 *  function that can modify the imported commands.
 *  Created On 12 August 2020
 */

import path from 'path'
import { promises as fs } from 'fs'

import eval from 'node-eval'

export default async function transform(
    file: string,
    transformer: (content: string) => Promise<string>,
): Promise<any> {
    // read the file
    const contents = (await fs.readFile(file, {
        encoding: 'UTF-8',
    })) as string

    // the variable that will be returned if executed successfully
    let code: any

    // pass it to the transformer if present
    if (transformer) {
        // prepare the file path
        const evalPath = path.format({
            ...path.parse(file),
            base: undefined,
            ext: '.js',
        })

        code = eval(await transformer(contents), evalPath).default
    } else {
        code = require(file).default
    }

    return code
}
