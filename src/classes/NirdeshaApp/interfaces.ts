import Discord from 'discord.js'
import Interaction from '../Interaction'

export default interface NirdeshaAppConfigImpl {
    name: string
    client: Discord.Client
    prefix: string
    interactions: Interaction[]
    tasks: string[]
    paths: {
        interactions: string
        tasks: string
    }
}
