/*
 *  This file will check if the provided command matches a valid interaction.
 *  Created On 12 August 2020
 */

import Discord from 'discord.js'
import NirdeshaApp from '..'
import Interaction from '../../Interaction'

export default async function matchInteraction(
    command: string,
    message: Discord.Message,
    app: NirdeshaApp,
): Promise<Interaction> {
    // check if the command includes any spaces
    const commandSplit = command.split(' ')

    // split the command into an array of words
    let currentIteration = commandSplit.length

    // the interaction when we find it
    let interaction: Interaction

    if (commandSplit.length > 1) {
        // if the command is more than one word, we remove each word and check
        // if it matches any of our defined interactions. This is done until there is
        // only one word to test.

        do {
            const commandStr = commandSplit.slice(0, currentIteration).join(' ')

            interaction = app.data.interactions.find(inter =>
                inter.data.command.startsWith(commandStr),
            )

            currentIteration--
        } while (currentIteration > 0 || !interaction)
    } else {
        // when there is only one word, we simply check if there is a defined
        // interaction available.

        interaction = app.data.interactions.find(inter =>
            inter.data.command.startsWith(command),
        )
    }

    // emit the noInteraction event if there isn't a known interaction
    // for the command
    if (!interaction) app.emit('noInteraction', command, message)

    return interaction
}
