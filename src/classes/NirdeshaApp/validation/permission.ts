/*
 *  This file will check if the command issuer has proper permissions
 *  to execute the command he/she issued.
 *  Created On 12 August 2020
 */

import Discord, { BitFieldResolvable, PermissionString } from 'discord.js'

import { forEach } from '../../../utilities/loops'
import Interaction from '../../Interaction'
import NirdeshaApp from '..'

export default async function hasPerms(
    interaction: Interaction,
    message: Discord.Message,
    app: NirdeshaApp,
): Promise<boolean> {
    // the variable that stores the access state across the iterations
    let hasAccess: boolean

    await forEach(
        interaction.data.restricts,
        async (role: BitFieldResolvable<PermissionString>) => {
            if (message.member.hasPermission(role)) {
                if (hasAccess !== true) hasAccess = true
            } else {
                hasAccess = false
            }
        },
    )

    // determine if the user has access or not
    if (!hasAccess) {
        app.emit('interactionAccessDenied', interaction.data.command, message)
        return false
    } else {
        return true
    }
}
