import Discord from 'discord.js';
import NirdeshaApp from './classes/NirdeshaApp';
import Interaction from './classes/Interaction/index';
declare function app(name: string, client: Discord.Client): Promise<NirdeshaApp>;
declare const _default: {
    app: typeof app;
    Interaction: typeof Interaction;
};
export default _default;
export { default as Interaction } from './classes/Interaction/index';
//# sourceMappingURL=nirdesha.d.ts.map