/// <reference types="node" />
import events from 'events';
import Discord from 'discord.js';
import NirdeshaAppConfigImpl from './interfaces';
declare interface NirdeshaApp {
    on(event: string, listener: Function): this;
    on(event: 'noInteraction', listener: (command: string, message: Discord.Message) => void): this;
    on(event: 'interactionAccessDenied', listener: (command: string, message: Discord.Message) => void): this;
    on(event: 'tooManyArguments', listener: (command: string, expected: number, got: number, message: Discord.Message) => void): this;
    on(event: 'insufficientArguments', listener: (command: string, expected: number, got: number, message: Discord.Message) => void): this;
}
declare class NirdeshaApp extends events.EventEmitter {
    data: NirdeshaAppConfigImpl;
    constructor(name: string, client: Discord.Client);
    interactions(path: string, extension?: string, transformer?: (content: string) => Promise<string>): Promise<this>;
    tasks(path: string): this;
}
export default NirdeshaApp;
//# sourceMappingURL=index.d.ts.map