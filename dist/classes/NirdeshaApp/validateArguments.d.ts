import Discord from 'discord.js';
import { InteractionConfigImpl } from '../Interaction/interfaces';
import NirdeshaApp from '.';
export default function validateArguments(interaction: InteractionConfigImpl, command: string, app: NirdeshaApp, message: Discord.Message): Promise<any>;
//# sourceMappingURL=validateArguments.d.ts.map