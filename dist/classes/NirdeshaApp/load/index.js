"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const path_1 = __importDefault(require("path"));
const glob_1 = require("glob");
const loops_1 = require("../../../utilities/loops");
const transform_1 = __importDefault(require("./transform"));
const index_1 = __importDefault(require("./validation/index"));
const arguments_1 = __importDefault(require("./validation/arguments"));
function load(directory, extension, transformer) {
    return __awaiter(this, void 0, void 0, function* () {
        const files = glob_1.sync(path_1.default.join(directory, '**', `*.${extension}`));
        const returnable = [];
        yield loops_1.forEach(files, (file) => __awaiter(this, void 0, void 0, function* () {
            const code = yield transform_1.default(file, transformer);
            yield index_1.default(code, file, directory);
            yield arguments_1.default(code, file, directory);
            returnable.push(code);
        }));
        return returnable;
    });
}
exports.default = load;
