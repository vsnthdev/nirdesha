"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const loops_1 = require("../../../../utilities/loops");
function args(code, file, directory) {
    return __awaiter(this, void 0, void 0, function* () {
        if (code.data.arguments.length < 1)
            return;
        yield loops_1.forEach(code.data.arguments, (argument) => __awaiter(this, void 0, void 0, function* () {
            if (!argument.name)
                throw Error(`The argument "${argument.identifier}" in "${file
                    .replace(directory, '')
                    .substring(1)}" doesn't have a name.`);
            if (argument.name.includes(' '))
                throw Error(`The argument "${argument.name}" in "${file
                    .replace(directory, '')
                    .substring(1)}" has invalid name.`);
            if (!argument.description)
                throw Error(`The argument "${argument.name}" in "${file
                    .replace(directory, '')
                    .substring(1)}" has no description.`);
            if (!argument.identifier)
                throw Error(`The argument "${argument.name}" in "${file
                    .replace(directory, '')
                    .substring(1)}" has no identifier.`);
            if (!argument.type)
                throw Error(`The argument "${argument.name}" in "${file
                    .replace(directory, '')
                    .substring(1)}" has no type.`);
            const possibleTypes = ['string', 'number'];
            if (!possibleTypes.includes(argument.type))
                throw Error(`The argument "${argument.name}" in "${file
                    .replace(directory, '')
                    .substring(1)}" has invalid type.`);
            if (argument.required == undefined)
                argument.required = true;
            const nextElement = code.data.arguments[code.data.arguments.indexOf(argument) + 1];
            if (nextElement) {
                if (argument.required == false && nextElement.required == true)
                    throw Error(`Cannot have required arguments after optional onces in "${file
                        .replace(directory, '')
                        .substring(1)}".`);
            }
            return;
        }));
        return;
    });
}
exports.default = args;
