"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const path_1 = __importDefault(require("path"));
const fs_1 = require("fs");
const node_eval_1 = __importDefault(require("node-eval"));
function transform(file, transformer) {
    return __awaiter(this, void 0, void 0, function* () {
        const contents = (yield fs_1.promises.readFile(file, {
            encoding: 'UTF-8',
        }));
        let code;
        if (transformer) {
            const evalPath = path_1.default.format(Object.assign(Object.assign({}, path_1.default.parse(file)), { base: undefined, ext: '.js' }));
            code = node_eval_1.default(yield transformer(contents), evalPath).default;
        }
        else {
            code = require(file).default;
        }
        return code;
    });
}
exports.default = transform;
