export default function transform(file: string, transformer: (content: string) => Promise<string>): Promise<any>;
//# sourceMappingURL=transform.d.ts.map