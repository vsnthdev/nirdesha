import Interaction from '../../Interaction';
export default function load(directory: string, extension: string, transformer: (content: string) => Promise<string>): Promise<Interaction[]>;
//# sourceMappingURL=index.d.ts.map