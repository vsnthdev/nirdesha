import Discord from 'discord.js';
import Interaction from '../../Interaction';
import NirdeshaApp from '..';
export default function hasPerms(interaction: Interaction, message: Discord.Message, app: NirdeshaApp): Promise<boolean>;
//# sourceMappingURL=permission.d.ts.map