"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const loops_1 = require("../../../utilities/loops");
function validateArguments(interaction, command, app, message) {
    return __awaiter(this, void 0, void 0, function* () {
        const uArgs = command.substr(interaction.command.length).trim().split(' ');
        if (uArgs.indexOf('') != -1)
            uArgs.splice(uArgs.indexOf(''), 1);
        const minimumArguments = interaction.arguments.filter(arg => arg.required == true).length;
        if (uArgs.length < minimumArguments) {
            app.emit('insufficientArguments', interaction.command, minimumArguments, uArgs.length, message);
            return;
        }
        const returnable = {};
        yield loops_1.forEach(interaction.arguments, (arg) => __awaiter(this, void 0, void 0, function* () {
            returnable[arg.name] = uArgs[0];
            uArgs.shift();
        }));
        if (uArgs.length > 0) {
            app.emit('tooManyArguments', interaction.command, interaction.arguments.length, uArgs.length + interaction.arguments.length, message);
            return;
        }
        return returnable;
    });
}
exports.default = validateArguments;
