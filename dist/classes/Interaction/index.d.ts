import Discord, { BitFieldResolvable, PermissionString } from 'discord.js';
import { InteractionConfigImpl, InteractionArgumentImpl } from './interfaces';
export default class Interaction {
    data: InteractionConfigImpl;
    constructor(command: string);
    description(desc: string): this;
    restrict(identifiers: BitFieldResolvable<PermissionString>[]): this;
    action(func: (message: Discord.Message, args: any) => Promise<boolean>): this;
    arguments(args: InteractionArgumentImpl[]): this;
}
//# sourceMappingURL=index.d.ts.map